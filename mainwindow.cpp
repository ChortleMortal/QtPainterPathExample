#include "mainwindow.h"
#include "arcData.h"
#include "layout_sliderset.h"

#include <cmath>

Widget::Widget(QWidget *parent) : QWidget(parent)
{
    width = 1.0;
    arect = QRectF(-600,-600,1200,1200);

    path_item   = new QGraphicsPathItem;
    path_item2  = new QGraphicsPathItem;
    circle_item = new QGraphicsEllipseItem;
    center_item = new QGraphicsEllipseItem;
    rect_item   = new QGraphicsRectItem;
    lineItem[0] = new QGraphicsLineItem;
    lineItem[1] = new QGraphicsLineItem;
    lineItem[2] = new QGraphicsLineItem;
    lineItem[3] = new QGraphicsLineItem;
    lineItem[4] = new QGraphicsLineItem;
    lineItem[5] = new QGraphicsLineItem;

    lineItem[0]->setPen(QPen(Qt::red,3));
    lineItem[1]->setPen(QPen(Qt::red,3));
    lineItem[2]->setPen(QPen(Qt::green,3));
    lineItem[3]->setPen(QPen(Qt::green,3));
    lineItem[4]->setPen(QPen(Qt::blue,1));
    lineItem[5]->setPen(QPen(Qt::blue,1));

    radius_slider = new SliderSet("Radius",100,0,2000);

    QRadioButton * rConvex  = new QRadioButton("Convex");
    QRadioButton * rConcave = new QRadioButton("Concave");
    QCheckBox * chkCircle   = new QCheckBox("Circle visibility");
    QCheckBox * chkRect     = new QCheckBox("Rectangle visibility");
                chkOther    = new QCheckBox("Other Arc");

    QHBoxLayout * hbox = new QHBoxLayout;
    hbox->addLayout(radius_slider);

    QHBoxLayout * hbox2 = new QHBoxLayout;
    hbox2->addWidget(rConvex);
    hbox2->addWidget(rConcave);
    hbox2->addWidget(chkCircle);
    hbox2->addWidget(chkRect);
    hbox2->addWidget(chkOther);
    hbox2->addStretch();

    scene = new QGraphicsScene(this);

    view  = new QGraphicsView(scene);
    view->scale(1, -1);
    view->setRenderHints(QPainter::Antialiasing);
    view->centerOn(arect.center());
    view->setScene(scene);

    QBrush nobrush(Qt::NoBrush);

    path_item->setBrush(nobrush);
    path_item->setPen(QPen(QColor("black"), width));

    path_item2->setBrush(nobrush);
    path_item2->setPen(QPen(QColor("green"), width));

    circle_item->setBrush(nobrush);
    circle_item->setPen(QPen(QColor("red"), width));

    center_item->setBrush(QBrush(QColor("blue")));
    center_item->setPen(QPen(QColor("blue"), 3));
    center_item->setRect(-5,-5,10,10);

    chkCircle->setChecked(true);
    chkRect->setChecked(true);
    chkOther->setChecked(false);

    rect_item->setBrush(Qt::NoBrush);
    rect_item->setPen(QPen(QColor(Qt::blue),width));

    connect(radius_slider,  &SliderSet::valueChanged, this, &Widget::updateRadius);
    connect(chkCircle,      &QCheckBox::toggled,   [this](bool checked){ circle_item->setVisible(checked); });
    connect(chkRect,        &QCheckBox::toggled,   [this](bool checked){ rect_item->setVisible(checked); });
    connect(chkOther,        &QCheckBox::toggled,  [this]{ updateDrawing(); });
    connect(rConvex,        &QCheckBox::toggled,   [this](bool checked){ convex = checked; updateDrawing(); });
    connect(rConcave,       &QCheckBox::toggled,   [this](bool checked){ convex = !checked; updateDrawing(); });

    scene->addItem(circle_item);
    scene->addItem(center_item);
    scene->addItem(rect_item);
    scene->addItem(lineItem[0]);
    scene->addItem(lineItem[1]);
    scene->addItem(lineItem[2]);
    scene->addItem(lineItem[3]);
    scene->addItem(lineItem[4]);
    scene->addItem(lineItem[5]);
    scene->addItem(path_item);
    scene->addItem(path_item2);

    QVBoxLayout *lay = new QVBoxLayout(this);
    lay->addLayout(hbox);
    lay->addLayout(hbox2);
    lay->addWidget(view);

    radius_slider->setValue(550);
    updateRadius(550);
    convex = false;
    if (convex)
        rConvex->setChecked(true);
    else
        rConcave->setChecked(true);
}

void Widget::updateRadius(int iradius)
{
    radius = iradius;
    updateDrawing();
}

void Widget::updateDrawing()
{
    QPointF v1(100,  100);      //top
    QPointF v2(0,      0);      // left
    QPointF v3(400, -100);      // bottom right

    // this does it
    QPainterPath path;

    path.moveTo(v2);
    path.lineTo(v3);

    arcData ad;
    ad.calcArcData(v3,v1,radius,convex);
    path.arcTo(ad.rect, ad.start, ad.span);

    path.lineTo(v2);

    // show circle and rectangle
    circle_item->setRect(ad.rect);
    rect_item->setRect(ad.rect);

    // show center
    qreal len = 5;
    QPointF center = ad.rect.center();
    lineItem[0]->setLine(QLineF(QPointF(center.x()-len,center.y()),    QPointF(center.x()+len,center.y())));
    lineItem[1]->setLine(QLineF(QPointF(center.x(),    center.y()-len),QPointF(center.x(),    center.y()+len)));

    QPainterPath path2;

    if (chkOther->isChecked())
    {
        path2.moveTo(v2);
        path2.lineTo(v3);

        arcData ad2;
        ad2.calcArcData(v3,v1,radius,!convex);
        path2.arcTo(ad2.rect, ad2.start, ad2.span);

        path2.lineTo(v2);

        QPointF center2 = ad2.rect.center();
        lineItem[2]->setLine(QLineF(QPointF(center2.x()-len,center2.y()),    QPointF(center2.x()+len,center2.y())));
        lineItem[3]->setLine(QLineF(QPointF(center2.x(),    center2.y()-len),QPointF(center2.x(),    center2.y()+len)));
        lineItem[4]->setLine(QLineF(v3,v1));
        lineItem[5]->setLine(QLineF(center,center2));
    }
    else
    {
        lineItem[2]->setLine(QLineF());
        lineItem[3]->setLine(QLineF());
        lineItem[4]->setLine(QLineF());
        lineItem[5]->setLine(QLineF());
    }

    update();

    path_item->setPath(path);
    path_item2->setPath(path2);

    scene->setSceneRect(arect);
    view->centerOn(arect.center());
}
