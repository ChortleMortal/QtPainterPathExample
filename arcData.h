#ifndef ARCDATA_H
#define ARCDATA_H

#include <QtGui>

class arcData
{
public:
    arcData();

    void calcArcData(const QPointF p1, const QPointF p2, const qreal radius, bool convex);
    void calcArcData(const QPointF p1, const QPointF p2, const QPointF c, bool convex);

    QRectF  rect;
    qreal   start;
    qreal   end;    // used internally
    qreal   span;

protected:
    QPointF getCenter(QPointF p1, QPointF p2, qreal radius, bool convex);
    qreal   getRadius(QPointF p1, QPointF p2, QPointF center);
    QRectF  getRect(QPointF center, qreal radius);
};

#endif // ARCDATA_H
