#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>
#include <QtGui>
#include "layout_sliderset.h"

class Widget: public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget * parent=nullptr);

private slots:
    void updateRadius(int iradius);

protected:
    void updateDrawing();

private:
    QGraphicsScene          *  scene;
    QGraphicsView           *  view;
    QGraphicsPathItem       *  path_item;
    QGraphicsPathItem       *  path_item2;
    QGraphicsEllipseItem    *  circle_item;
    QGraphicsEllipseItem    *  center_item;
    QGraphicsRectItem       *  rect_item;
    QGraphicsLineItem       *  lineItem[6];
    SliderSet               *  radius_slider;

    QCheckBox               * chkOther;

    bool    convex;
    qreal   radius;
    qreal   width;      // pen width

    QRectF  arect;
};

#endif
