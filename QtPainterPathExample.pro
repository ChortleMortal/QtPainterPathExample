QT += core gui widgets

CONFIG += c++17

SOURCES += \
    arcdata.cpp \
    layout_sliderset.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    arcData.h \
    layout_sliderset.h \
    mainwindow.h

