#include "arcdata.h"

arcData::arcData()
{
}

void arcData::calcArcData(const QPointF p1, const QPointF p2, const qreal radius, bool convex)
{
    //qDebug() << p1 << p2 << radius;

    QPointF center = getCenter(p1,p2,radius,convex);
    rect           = getRect(center,radius);

    //qDebug() << "center" << center;
    //qDebug() << "rect" << rect;

    start = QLineF(center, p1).angle();
    end   = QLineF(center, p2).angle();
    span  = end - start;

    if (span > 180.0)
    {
        span  = 360.0 - span;
    }
    if (span  > 0)
    {
        span = -span;
    }
    if (!convex)
    {
        span = -span;
    }

    qDebug() << (convex ? "Convex " : "Concave") << "arcData" << rect  << "center" << center << start << end << "span=" << span;
}

void arcData::calcArcData(const QPointF p1, const QPointF p2, const QPointF c, bool convex)
{
    qreal radius = getRadius(p1,p2,c);
    calcArcData(p1,p2,radius,convex);
}

QPointF arcData::getCenter(QPointF p1, QPointF p2, qreal radius, bool convex)
{
    static QPointF lastCenter;

    QLineF edge(p1,p2);

    QPointF vp1 = edge.pointAt(0.5);

    // calc normal vector
    QLineF baseline(vp1,p2);
    QPointF vp2;
    if (convex)
    {
        vp2.setX(-baseline.dy());
        vp2.setY(baseline.dx());
    }
    else
    {
        vp2.setX(baseline.dy());
        vp2.setY(-baseline.dx());
    }


    // adjust len using Pythagoras
    qreal edgelen = edge.length() / 2.0;
    qreal len2    = (radius * radius) - (edgelen * edgelen);
    if (len2 > 0)
    {
        qreal len     = qSqrt(len2);
        QLineF normalVector(vp1,vp1+vp2);
        normalVector.setLength(len);
        lastCenter = normalVector.p2();
    }
    return lastCenter;   // the center
}

qreal arcData::getRadius(QPointF p1, QPointF p2, QPointF center)
{
#if 0
    QLineF edge(p1,p2);
    QPointF mid = edge.pointAt(0.5);
    qreal w = edge.length() / 2.0;
    QLineF h(mid,center);

    qreal radius = qSqrt((w *w) + (h.length() * h.length()));
    return radius;
#else
    Q_UNUSED(p2)
    return QLineF(center,p1).length();
#endif
}

QRectF arcData::getRect(QPointF center, qreal radius)
{
    return QRectF(QPointF(center.x()-radius,center.y()-radius),QSize(radius*2.0, radius*2.0));
}
